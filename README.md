# API

## OG�LNE

### Command: `uruchom tablic�`
```
DashboardFacade *db;
db = new DashboardFacade();
```

## STEROWANIE

### Boolean: `joystick jest 'stateName'`

```
bool isJoystickState(uint8_t stateName);
db->isJoystickState(/*{joystickState}*/)

stateName:
    JOYSTICK_STATE_MOVED - uruchomiony
    JOYSTICK_STATE_PRESS - wci�ni�ty
    JOYSTICK_STATE_UP    - przesuni�ty do g�ry
    JOYSTICK_STATE_DOWN  - przesuni�ty w d�
    JOYSTICK_STATE_LEFT  - przesuni�ty w lewo
    JOYSTICK_STATE_RIGHT - przesuni�ty w prawo

default: JOYSTICK_STATE_MOVED
```

### Boolean: `przycisk 'buttonName' jest wci�ni�ty`

```
bool isButtonPress(uint8_t buttonName);
db->isButtonPress(/*{buttonName}*/)

buttonName:
    BUTTON_YELLOW - ��ty
    BUTTON_RED    - czerwony
    BUTTON_BLUE   - niebieski
    BUTTON_GREEN  - zielony
    BUTTON_TOUCH  - dotykowy

default: BUTTON_TOUCH
```

## CZUJNIKI

### Boolean: `czujnik 'sensorName' jest aktywny`

```
bool isSensorEnabled(uint8_t sensorName);
db->getJoystickState(/*{sensorName}*/)

sensorName:
    SENSOR_FLAME        - p�omienia
    SENSOR_HALL         - Halla
    SENSOR_LDR          - fotorezystor
    SENSOR_OPTOCOUPLER  - transoptor
    SENSOR_PIR          - ruchu
    SENSOR_REED         - kontaktron
    SENSOR_REFLECTION   - odbiciowy
    SENSOR_SOUND        - d�wi�ku
    SENSOR_VIBRATION    - wibracji
    SENSOR_TILT         - przechylenia

default: SENSOR_PIR
```

### Number: `Czujnik: zmierz 'measurementType'`

```
int getSensorValue(uint8_t measurementType);
getSensorValue(/*{measurementType}*/)

measurementType:
    SENSOR_DHT_TEMPERATURE - temperatur�
    SENSOR_DHT_HUMIDITY    - wilgotno��
    SENSOR_SONAR_DISTANCE  - odleg�o��

default: SENSOR_DHT_TEMPERATURE
```

## WY�WIETLACZE

### Command: `Ustaw 'ledColor' dla LED nr 'ledNo' na 'ledType'`

```
void showPixelColor(uint8_t ledNo, uint8_t ledColor, uint8_t ledType);
db->showPixelColor(/*{ledNo}*/, /*{ledColor}*/, /*{ledType}*/);

ledNo:
    1, 2, ..., 8
    1, 2, ..., 16
ledColor:
    LED_COLOR_BLACK     - wy��czony
    LED_COLOR_WHITE     - bia�y
    LED_COLOR_RED       - czerwony
    LED_COLOR_GREEN     - zielony
    LED_COLOR_BLUE      - niebieski
    LED_COLOR_YELLOW    - ��ty
    LED_COLOR_CYAN      - cyjanowy
    LED_COLOR_MAGNETA   - magneta
    LED_COLOR_MAROON    - bordowy
    LED_COLOR_PURPLE    - fioletowy
ledType:
    LED_TYPE_LINE   - linii
    LED_TYPE_CIRCLE - okr�gu

default:
    ledNo    - 1
    ledColor - LED_COLOR_WHITE
    ledType  - LED_TYPE_LINE
```

### Wy�wietlacz LCD

#### Command: `LCD: wyczy�� wy�wietlacz`

```
db->lcd->clear();
```

#### Command: `LCD: ustaw kursor w wierszu 'lcdRowNo' i kolumnie 'lcdColumnNo'`

```
db->lcd->setCursor(/*{lcdColumnNo}*/, /*{lcdRowNo}*/);

lcdRowNo: 1, 2
lcdColumnNo: 1, 2, ..., 16
```
#### Command: `LCD: poka� tekst 'lcdString'`

```
db->lcd->print(/*{lcdString}*/);

lcdString: tekst
```

## ELEMENTY WYKONAWCZE

### Command: `Servo: wska� 'servoAngle'`

```
void setServoAngle(uint8_t servoAngle);
db->setServoAngle(/*{servoAngle}*/);

angle:
    SERVO_ICON_HAPPY        - zadowolenie
    SERVO_ICON_LAUGH        - �miech
    SERVO_ICON_SAD          - smutek
    SERVO_ICON_TOUNGUE      - j�zyk
    SERVO_ICON_SURPRISED    - zaskoczenie
    SERVO_ICON_YAWN         - ziew
    SERVO_ICON_BRAVE        - odwaga

default: SERVO_ICON_HAPPY
```

### Command: `Silnik: obracaj 'motorSpeed' w 'motorDirection' przez 'motorOnTime' sekund`
```
void runDcMotor(uint8_t mode, uint8_t speed, uint8_t onTime);
db->runDcMotor(/*{motorDirection}*/, /*{motorSpeed}*/, /*{motorOnTime}*/);

motorSpeed:
    DC_MOTOR_SPEED_SLOW     - wolno
    DC_MOTOR_SPEED_MEDIUM   - �rednio
    DC_MOTOR_SPEED_FAST     - szybko

motorDirection:
    DC_MOTOR_RIGHT - prawo
    DC_MOTOR_LEFT  - lewo

motorOnTime: 1, 2, n

default:
   mode             - DC_MOTOR_SPEED_FAST
   motorDirection   - DC_MOTOR_RIGHT
   motorOnTime      - 3
```

### Command: `Elektromagnes 'electromagnetType' uruchom na 'electromagnetOnTime' sekund`
```
void runElectromagnet(uint8_t type, unsigned long onTime);
db->runElectromagnet(/*{electromagnetType}*/, /*{electromagnetOnTime}*/);

electromagnetType:
    ELECTROMAGNET_HOLDING  - trzymaj�cy
    ELECTROMAGNET_PULLING - ci�gn�cy

electromagnetOnTime: 1, 2, ..., 10

default:
   type - ELECTROMAGNET_HOLDING
   electromagnetOnTime - 3
```

### Command: `Piezzo: w��cz 'buzzerSoundType' d�wi�k przez 'buzzerOnTime' sekund`
```
void runBuzzer(uint8_t type, uint8_t onTime);
db->runBuzzer(/*{buzzerSoundType}*/, /*{buzzerOnTime}*/);

buzzerSoundType:
    BUZZER_SOUND_LOW        - niski
    BUZZER_SOUND_MEDIUM     - �redni
    BUZZER_SOUND_HIGH       - wysoki

buzzerOnTime - 1, 2, n

default:
    type    - BUZZER_SOUND_LOW
    onTime  - 1
```
