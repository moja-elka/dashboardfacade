#ifndef DashboardFacade_h
#define DashboardFacade_h

#include <Arduino.h>
#include "Adafruit_NeoPixel.h"
#include "Adafruit_TiCoServo.h"
#include "LiquidCrystal_I2C.h"
#include "dsg_button.h"
#include "dsg_gauge.h"
#include "dsg_ky-023.h"
#include "NewPing.h"
#include "SimpleDHT.h"
#include "MX1508.h"

class DashboardFacade {
    public:
		//PINS - digital
		//displays
        static const uint16_t PIN_LED_LINE = 22;
        static const uint16_t PIN_LED_CIRCLE = 23;

        //controls
        static const int PIN_JOYSTICK_BUTTON = 24;
        static const uint8_t PIN_BUTTON_TOUCH = 25;
        static const uint8_t PIN_BUTTON_YELLOW = 26;
        static const uint8_t PIN_BUTTON_RED = 27;
        static const uint8_t PIN_BUTTON_BLUE = 28;
        static const uint8_t PIN_BUTTON_GREEN = 29;

        //sensors
        static const uint8_t PIN_SENSOR_ULTRASONIC_ECHO = 30;
        static const uint8_t PIN_SENSOR_ULTRASONIC_TRIG = 31;
        static const uint8_t PIN_SENSOR_FLAME = 32;
        static const uint8_t PIN_SENSOR_VIBRATION = 33;

        static const uint8_t PIN_SENSOR_TILT = 34;
        static const uint8_t PIN_SENSOR_HALL = 35;
        static const uint8_t PIN_SENSOR_LDR = 36;
        static const uint8_t PIN_SENSOR_REFLECTION = 37;

        static const uint8_t PIN_SENSOR_OPTOCOUPLER = 38;
        static const uint8_t PIN_SENSOR_PIR = 39;
        static const uint8_t PIN_SENSOR_SOUND = 40;
        static const uint8_t PIN_SENSOR_DHT11 = 41;
        static const uint8_t PIN_SENSOR_REED = 42;

        //executive
        static const uint8_t PIN_BUZZER = 5;        //PWM
        static const uint8_t PIN_SERVO = 44;        //PWM
        static const uint8_t PIN_MOTOR_IN_1 = 4;    //PWM
        static const uint8_t PIN_MOTOR_IN_2 = 13;   //PWM
        static const uint8_t PIN_EM_PULL = 47;
        static const uint8_t PIN_EM_HOLDING = 48;

        //PINS - analog
        static const int PIN_JOYSTICK_X = A0;
        static const int PIN_JOYSTICK_Y = A1;

		//LCD
		static const uint8_t LCD_ADDRESS = 0x27;
		static const uint8_t LCD_COLS = 16;
		static const uint8_t LCD_ROWS = 2;

		//LED lines
        static const uint8_t LED_TYPE_LINE = 0;
        static const uint8_t LED_TYPE_CIRCLE = 1;
		static const uint16_t LED_LINE_COUNT = 8;
        static const uint16_t LED_CIRCLE_COUNT = 16;
        const uint8_t LED_COLORS[10][3] = {
            {0,0,0},   		//Black/Off
            {255,255,255},  //White
            {255,0,0},   	//Red
            {0,255,0},    	//Green
            {0,0,255},    	//Blue
            {255,255,0},    //Yellow
            {0,255,255},   	//Cyan
            {255,0,255},    //Magenta
            {128,0,0},    	//Maroon
            {128,0,128}    	//Purple
        };
        static const uint8_t LED_COLOR_BLACK = 0;
        static const uint8_t LED_COLOR_WHITE = 1;
        static const uint8_t LED_COLOR_RED = 2;
        static const uint8_t LED_COLOR_GREEN = 3;
        static const uint8_t LED_COLOR_BLUE = 4;
        static const uint8_t LED_COLOR_YELLOW = 5;
        static const uint8_t LED_COLOR_CYAN = 6;
        static const uint8_t LED_COLOR_MAGNETA = 7;
        static const uint8_t LED_COLOR_MAROON = 8;
        static const uint8_t LED_COLOR_PURPLE = 9;
        static const uint8_t LED_BRIGHTNESS = 100;

        //JOYSTICK
        // X Axis
        static const uint16_t KY012_I_MIN = 63;
        static const uint16_t KY012_I_0 = 517;
        static const uint16_t KY012_I_MAX = 960;
        static const int8_t KY012_X_MIN = -30;
        static const int8_t KY012_X_0 = 0;
        static const int8_t KY012_X_MAX = 30;
        // Y Axis
        static const uint16_t KY012_J_MIN = 63;
        static const uint16_t KY012_J_0 = 523;
        static const uint16_t KY012_J_MAX = 960;
        static const int8_t KY012_Y_MIN = -30;
        static const int8_t KY012_Y_0 = 0;
        static const int8_t KY012_Y_MAX = 30;
        // States
        static const uint8_t JOYSTICK_STATE_MOVED = 0;
        static const uint8_t JOYSTICK_STATE_PRESS = 1;
        static const uint8_t JOYSTICK_STATE_UP = 2;
        static const uint8_t JOYSTICK_STATE_DOWN = 3;
        static const uint8_t JOYSTICK_STATE_LEFT = 4;
        static const uint8_t JOYSTICK_STATE_RIGHT = 5;

        //BUTTONS
        static const uint16_t BUTTON_DEBOUNCE = 25; //time in ms
        static const uint8_t BUTTON_YELLOW = 0;
        static const uint8_t BUTTON_RED = 1;
        static const uint8_t BUTTON_BLUE = 2;
        static const uint8_t BUTTON_GREEN = 3;
        static const uint8_t BUTTON_TOUCH = 4;

        //SENSORS
        static const uint8_t SENSOR_FLAME = 0;
        static const uint8_t SENSOR_HALL = 1;
        static const uint8_t SENSOR_LDR = 2;
        static const uint8_t SENSOR_OPTOCOUPLER = 3;
        static const uint8_t SENSOR_PIR = 4;
        static const uint8_t SENSOR_REED = 5;
        static const uint8_t SENSOR_REFLECTION = 6;
        static const uint8_t SENSOR_VIBRATION = 7;
        static const uint8_t SENSOR_TILT = 8;
        static const uint8_t SENSOR_SOUND = 9;

        //EXECUTIVES
        static const uint8_t SERVO_ICON_HAPPY = 165;
        static const uint8_t SERVO_ICON_LAUGH = 150;
        static const uint8_t SERVO_ICON_SAD = 120;
        static const uint8_t SERVO_ICON_TOUNGUE = 90;
        static const uint8_t SERVO_ICON_SURPRISED = 60;
        static const uint8_t SERVO_ICON_YAWN = 30;
        static const uint8_t SERVO_ICON_BRAVE = 15;
        static const uint16_t SERVO_MIN = 500; // 0.5 ms pulse
        static const uint16_t SERVO_MAX = 2400; // 2.4 ms pulse

        static const uint8_t DC_MOTOR_LEFT = 0;
        static const uint8_t DC_MOTOR_RIGHT = 1;
        static const uint8_t DC_MOTOR_SPEED_SLOW = 0;
        static const uint8_t DC_MOTOR_SPEED_MEDIUM = 1;
        static const uint8_t DC_MOTOR_SPEED_FAST = 2;
        static const uint8_t DC_MOTOR_SPEED_SLOW_VALUE = 40;
        static const uint8_t DC_MOTOR_SPEED_MEDIUM_VALUE = 150;
        static const uint8_t DC_MOTOR_SPEED_FAST_VALUE = 255;

        static const uint8_t ELECTROMAGNET_HOLDING = 0;
        static const uint8_t ELECTROMAGNET_PULLING = 1;

        static const uint8_t BUZZER_SOUND_LOW = 0;
        static const uint8_t BUZZER_SOUND_MEDIUM = 1;
        static const uint8_t BUZZER_SOUND_HIGH = 2;
        static const unsigned int BUZZER_SOUND_LOW_VALUE = 800;
        static const unsigned int BUZZER_SOUND_MEDIUM_VALUE = 2000;
        static const unsigned int BUZZER_SOUND_HIGH_VALUE = 6000;

        //DHT config
        static const uint8_t SENSOR_DHT_TEMPERATURE = 0;
        static const uint8_t SENSOR_DHT_HUMIDITY = 1;
        unsigned long lastDhtMeasureTime = 0;
        int dhtTemperature = 0;
        int dhtHumidity = 0;

        //Sonar config
        static const uint8_t SENSOR_SONAR_DISTANCE = 3;
        unsigned long lastUltrasonicMeasureTime = 0;
        int sonarLength = 0;

        //Electromagnet config
        unsigned long electromagnetPullingLastRunTime = 0;
        uint8_t electromagnetPullingIsOn = 0;
        uint8_t electromagnetPullingOnTime = 0;

        unsigned long electromagnetHoldingLastRunTime = 0;
        uint8_t electromagnetHoldingIsOn = 0;
        uint8_t electromagnetHoldingOnTime = 0;

        //Motor config
        unsigned long motorLastRunTime = 0;
        uint8_t motorIsOn = 0;
        uint8_t motorOnTime = 0;

        //Buzzer config
        unsigned long buzzerLastRunTime = 0;
        uint8_t buzzerIsOn = 0;
        uint8_t buzzerOnTime = 0;

        LiquidCrystal_I2C *lcd;
        Adafruit_NeoPixel *ledLine;
        Adafruit_NeoPixel *ledCircle;
        DSG_Ky023 *joystick;
        DSG_Button *btnYellow;
        DSG_Button *btnRed;
        DSG_Button *btnBlue;
        DSG_Button *btnGreen;
        SimpleDHT11 *dht;
        NewPing *sonar;
        Adafruit_TiCoServo *servo;
        MX1508 *motor;

        DashboardFacade();
        void initDisplays();
        void setLedBarOneColor(uint8_t ledNo, uint8_t ledColor, uint8_t ledType);
        void setLedBarAllColor(uint8_t ledColor, uint8_t ledType);
        void clearLedBar(uint8_t ledType);

        void initControls();
        bool isJoystickState(uint8_t stateName);
        bool isButtonPress(uint8_t buttonName);

        void initSensors();
        bool isSensorEnabled(uint8_t sensorName);
        int getSensorValue(uint8_t measurementType);

        void initExecutiveElements();
        void setServoAngle(uint8_t angle);
		void runDcMotor(uint8_t mode, uint8_t speed, uint8_t onTime);
        void runElectromagnet(uint8_t type, uint8_t onTime);
        void runBuzzer(uint8_t type, uint8_t onTime);

        void checkExecutiveCounters();
};

#endif
