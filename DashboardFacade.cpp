#include "DashboardFacade.h"

DashboardFacade::DashboardFacade()
{
    lcd = new LiquidCrystal_I2C(LCD_ADDRESS, LCD_COLS, LCD_ROWS);
    ledLine = new Adafruit_NeoPixel(LED_LINE_COUNT, PIN_LED_LINE, NEO_GRB + NEO_KHZ800);
    ledCircle = new Adafruit_NeoPixel(LED_CIRCLE_COUNT, PIN_LED_CIRCLE, NEO_GRB + NEO_KHZ800);
    joystick = new DSG_Ky023(PIN_JOYSTICK_X, PIN_JOYSTICK_Y, PIN_JOYSTICK_BUTTON, BUTTON_DEBOUNCE);
    btnYellow = new DSG_Button(PIN_BUTTON_YELLOW, BUTTON_DEBOUNCE);
    btnRed = new DSG_Button(PIN_BUTTON_RED, BUTTON_DEBOUNCE);
    btnBlue = new DSG_Button(PIN_BUTTON_BLUE, BUTTON_DEBOUNCE);
    btnGreen = new DSG_Button(PIN_BUTTON_GREEN, BUTTON_DEBOUNCE);
    dht = new SimpleDHT11(PIN_SENSOR_DHT11);
    sonar = new NewPing(PIN_SENSOR_ULTRASONIC_TRIG, PIN_SENSOR_ULTRASONIC_ECHO, 50); //TODO move 50 to const
    servo = new Adafruit_TiCoServo();
    motor = new MX1508(PIN_MOTOR_IN_1, PIN_MOTOR_IN_2);

    initDisplays();
    initControls();
    initSensors();
    initExecutiveElements();
}

 void DashboardFacade::initDisplays()
 {
    lcd->init();
    lcd->backlight();
    lcd->print("Dashboard v1");
    ledLine->begin();
    ledLine->setBrightness(LED_BRIGHTNESS);
    ledLine->clear();
    ledLine->show();
    ledCircle->begin();
    ledCircle->setBrightness(LED_BRIGHTNESS);
    ledCircle->clear();
    ledCircle->show();
 }

 void DashboardFacade::initControls()
 {
     //Buttons
    btnYellow->begin();
    btnRed->begin();
    btnBlue->begin();
    btnGreen->begin();
    //Joystick
    pinMode(PIN_JOYSTICK_X, INPUT);
    pinMode(PIN_JOYSTICK_Y, INPUT);
    pinMode(PIN_JOYSTICK_BUTTON, INPUT_PULLUP);
    joystick->calibrateX(KY012_I_MIN, KY012_I_0, KY012_I_MAX, KY012_X_MIN, KY012_X_0, KY012_X_MAX);
    joystick->calibrateY(KY012_J_MIN, KY012_J_0, KY012_J_MAX, KY012_Y_MIN, KY012_Y_0, KY012_Y_MAX);
    joystick->begin();
    //Touch sensor
    pinMode(PIN_BUTTON_TOUCH, INPUT);
 }

 void DashboardFacade::initSensors()
 {
    pinMode(PIN_SENSOR_FLAME, INPUT);
    pinMode(PIN_SENSOR_HALL, INPUT); //used digital output
    pinMode(PIN_SENSOR_LDR, INPUT);
    pinMode(PIN_SENSOR_OPTOCOUPLER, INPUT);
    pinMode(PIN_SENSOR_PIR, INPUT);
    pinMode(PIN_SENSOR_REED, INPUT); //used digital output
    pinMode(PIN_SENSOR_REFLECTION, INPUT);
    pinMode(PIN_SENSOR_VIBRATION, INPUT);
    pinMode(PIN_SENSOR_TILT, INPUT);
    pinMode(PIN_SENSOR_SOUND, INPUT);
}

 void DashboardFacade::initExecutiveElements()
 {
    servo->attach(PIN_SERVO);
    setServoAngle(SERVO_ICON_BRAVE);

    pinMode(PIN_BUZZER, OUTPUT);

    pinMode(PIN_EM_PULL, OUTPUT);
    pinMode(PIN_EM_HOLDING, OUTPUT);

    //motor->standby();
 }

/**
* Set and show one of defined colors (ledColor) for given led no (ledNo) and led display (ledType)
*/
void DashboardFacade::setLedBarOneColor(uint8_t ledNo, uint8_t ledColor, uint8_t ledType)
{
    if (LED_TYPE_LINE == ledType && ledNo >= 0 && ledNo < LED_LINE_COUNT) {
        ledLine->setPixelColor(ledNo, LED_COLORS[ledColor][0], LED_COLORS[ledColor][1], LED_COLORS[ledColor][2]);
        ledLine->show();
    } else if (LED_TYPE_CIRCLE == ledType && ledNo >= 0 && ledNo < LED_CIRCLE_COUNT) {
        ledCircle->setPixelColor(LED_CIRCLE_COUNT - 1 - ledNo, LED_COLORS[ledColor][0], LED_COLORS[ledColor][1], LED_COLORS[ledColor][2]);
        ledCircle->show();
    }
}

/**
* Set and show one of defined colors (ledColor) for all LEDs
*/
void DashboardFacade::setLedBarAllColor(uint8_t ledColor, uint8_t ledType)
{
    if (LED_TYPE_LINE == ledType) {
        ledLine->fill(ledLine->Color(LED_COLORS[ledColor][0], LED_COLORS[ledColor][1], LED_COLORS[ledColor][2], LED_BRIGHTNESS), 0, LED_LINE_COUNT);
        ledLine->show();
    } else if (LED_TYPE_CIRCLE == ledType) {
        ledCircle->fill(ledLine->Color(LED_COLORS[ledColor][0], LED_COLORS[ledColor][1], LED_COLORS[ledColor][2], LED_BRIGHTNESS), 0, LED_CIRCLE_COUNT);
        ledCircle->show();
    }
}

/**
* Disable given led bar
*/
void DashboardFacade::clearLedBar(uint8_t ledType)
{
    if (LED_TYPE_LINE == ledType) {
        ledLine->clear();
        ledLine->show();
    } else if (LED_TYPE_CIRCLE == ledType) {
        ledCircle->clear();
        ledCircle->show();
    }
}

/*
* Read joystick state
* https://github.com/mr700/DSG_KY-023
*/
bool DashboardFacade::isJoystickState(uint8_t stateName)
{
    joystick->read();

    if (JOYSTICK_STATE_MOVED == stateName) {
        return joystick->getBtn()
            || joystick->getX() < -1
			|| joystick->getX() > 1
            || joystick->getY() < -1
			|| joystick->getY() > 1;
    } else if (JOYSTICK_STATE_PRESS == stateName) {
        return joystick->getBtn();
    } else if (JOYSTICK_STATE_UP == stateName) {
        return joystick->getX() < -1;
    } else if (JOYSTICK_STATE_DOWN == stateName) {
        return joystick->getX() > 1;
    } else if (JOYSTICK_STATE_LEFT == stateName) {
        return joystick->getY() > 1;
    } else if (JOYSTICK_STATE_RIGHT == stateName) {
        return joystick->getY() < -1;
    }
}

/*
* Check if one of the buttons is press
* https://www.arduino.cc/en/tutorial/button
*/
bool DashboardFacade::isButtonPress(uint8_t buttonName)
{
    if (BUTTON_YELLOW == buttonName) {
        btnYellow->read();
        return btnYellow->get();
    } else if (BUTTON_RED == buttonName) {
        btnRed->read();
        return btnRed->get();
    } else if (BUTTON_BLUE == buttonName) {
        btnBlue->read();
        return btnBlue->get();
    } else if (BUTTON_GREEN == buttonName) {
        btnGreen->read();
        return btnGreen->get();
    } else if (BUTTON_TOUCH == buttonName) {
        return HIGH == digitalRead(PIN_BUTTON_TOUCH);
    }
}

bool DashboardFacade::isSensorEnabled(uint8_t sensorName)
{
    if (SENSOR_FLAME == sensorName) {
        return LOW == digitalRead(PIN_SENSOR_FLAME);
    } else if (SENSOR_HALL == sensorName) {
        return LOW == digitalRead(PIN_SENSOR_HALL);
    } else if (SENSOR_LDR == sensorName) {
        return LOW == digitalRead(PIN_SENSOR_LDR);
    } else if (SENSOR_OPTOCOUPLER == sensorName) {
        return LOW == digitalRead(PIN_SENSOR_OPTOCOUPLER);
    } else if (SENSOR_PIR == sensorName) {
        //https://randomnerdtutorials.com/arduino-with-pir-motion-sensor/
        return HIGH == digitalRead(PIN_SENSOR_PIR);
    } else if (SENSOR_REED == sensorName) {
        //https://arduinomodules.info/ky-025-reed-switch-module/
        return LOW == digitalRead(PIN_SENSOR_REED);
    } else if (SENSOR_REFLECTION == sensorName) {
        return HIGH == digitalRead(PIN_SENSOR_REFLECTION);
    } else if (SENSOR_VIBRATION == sensorName) {
        //https://create.arduino.cc/projecthub/albertoz/vibration-sensor-module-c88067
        return HIGH == digitalRead(PIN_SENSOR_VIBRATION);
    } else if (SENSOR_TILT == sensorName) {
        return HIGH == digitalRead(PIN_SENSOR_TILT);
    } else if (SENSOR_SOUND == sensorName) {
        return LOW == digitalRead(PIN_SENSOR_SOUND);
    }
}

//https://github.com/winlinvip/SimpleDHT
 int DashboardFacade::getSensorValue(uint8_t measurementType)
 {
     byte temperature = 0;
     byte humidity = 0;

     if (millis() - lastDhtMeasureTime > 1000) {
         //DHT11 sampling rate is 1HZ.
         int err = SimpleDHTErrSuccess;

         if ((err = dht->read(&temperature, &humidity, NULL)) == SimpleDHTErrSuccess) {
             dhtTemperature = (int) temperature;
             dhtHumidity = (int) humidity;
             lastDhtMeasureTime = millis();
         }
     }

     if (SENSOR_DHT_TEMPERATURE == measurementType) {
         return dhtTemperature;
     } else if (SENSOR_DHT_HUMIDITY == measurementType) {
         return dhtHumidity;
     } else if (SENSOR_SONAR_DISTANCE == measurementType) {
         if (millis() - lastUltrasonicMeasureTime > 1000) {
             sonarLength = sonar->ping_cm();
             lastUltrasonicMeasureTime = millis();
         }

         return sonarLength;
     }
 }

void DashboardFacade::setServoAngle(uint8_t angle)
{
    int x;
    x = map(angle, 0, 180, SERVO_MIN, SERVO_MAX);
    servo->write(x);
}

void DashboardFacade::runDcMotor(uint8_t mode, uint8_t speedLevel, uint8_t onTime)
{
    if (motorIsOn) {
        return;
    }

    motorOnTime = onTime;
    uint8_t speed = DC_MOTOR_SPEED_FAST_VALUE; //default (255)

    if (DC_MOTOR_SPEED_SLOW == speedLevel) {
        speed = DC_MOTOR_SPEED_SLOW_VALUE;
    } else if (DC_MOTOR_SPEED_MEDIUM == speedLevel) {
        speed = DC_MOTOR_SPEED_MEDIUM_VALUE;
    }

    if (DC_MOTOR_LEFT == mode) {
        motor->drive(-speed);
     } else if (DC_MOTOR_RIGHT == mode) {
        motor->drive(speed);
    }

    motorIsOn = 1;
    motorLastRunTime = millis();
}

void DashboardFacade::runElectromagnet(uint8_t type, uint8_t onTime)
{
    if (ELECTROMAGNET_HOLDING == type && !electromagnetHoldingIsOn) {
        digitalWrite(PIN_EM_HOLDING, HIGH);
        electromagnetHoldingIsOn = 1;
        electromagnetHoldingLastRunTime = millis();
        electromagnetHoldingOnTime = onTime;
    } else if (ELECTROMAGNET_PULLING == type && !electromagnetPullingIsOn) {
        digitalWrite(PIN_EM_PULL, HIGH);
        electromagnetPullingIsOn = 1;
        electromagnetPullingLastRunTime = millis();
        electromagnetPullingOnTime = onTime;
    }
}

void DashboardFacade::runBuzzer(uint8_t type, uint8_t onTime)
{
    if (buzzerIsOn) {
        return;
    }

    unsigned int frequency = BUZZER_SOUND_LOW_VALUE; //default 800Hz;

    if (BUZZER_SOUND_MEDIUM == type) {
        frequency = BUZZER_SOUND_MEDIUM_VALUE;
    } else if (BUZZER_SOUND_HIGH == type) {
        frequency = BUZZER_SOUND_HIGH_VALUE;
    }

    tone(PIN_BUZZER, frequency);
    buzzerLastRunTime = millis();
    buzzerIsOn = 1;
}

void DashboardFacade::checkExecutiveCounters()
{
    if (motorIsOn && millis() - motorLastRunTime > motorOnTime * 1000UL) {
        motor->brake();
        motorIsOn = 0;
    }

    if (electromagnetHoldingIsOn && millis() - electromagnetHoldingLastRunTime > electromagnetHoldingOnTime * 1000UL) {
        digitalWrite(PIN_EM_HOLDING, LOW);
        electromagnetHoldingIsOn = 0;
    }

    if (electromagnetPullingIsOn && millis() - electromagnetPullingLastRunTime > electromagnetPullingOnTime * 1000UL) {
        digitalWrite(PIN_EM_PULL, LOW);
        electromagnetPullingIsOn = 0;
    }

    if (buzzerIsOn && millis() - buzzerLastRunTime > buzzerOnTime * 1000UL) {
        noTone(PIN_BUZZER);
        buzzerIsOn = 0;
    }
}
